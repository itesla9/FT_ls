/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   making_chain.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/23 20:00:12 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/23 20:03:02 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void	ft_not_files(t_a **start_not_files)
{
	ft_print_not_files(*start_not_files);
	ft_clean_list(start_not_files);
}

void	ft_sort_not_files(t_a **start, t_a *elem)
{
	t_a *ptr;
	t_a *crawler;

	ptr = *start;
	crawler = *start;
	while (ptr)
	{
		if (ft_strcmp(ptr->s, elem->s) > 0)
		{
			ft_add_to_sort(ptr, &elem, crawler, start);
			return ;
		}
		ptr = ptr->next;
	}
	ptr = *start;
	while (ptr->next)
		ptr = ptr->next;
	ptr->next = elem;
}

void	ft_make_chain_not_files(t_a **rocket, char *s)
{
	t_a *elem;

	if (*rocket != NULL)
	{
		elem = (t_a *)malloc(sizeof(t_a));
		elem->s = ft_strdup(s);
		elem->path = ft_strdup(s);
		elem->next = NULL;
		ft_sort_not_files(rocket, elem);
	}
	if (*rocket == NULL)
	{
		*rocket = (t_a *)malloc(sizeof(t_a));
		(*rocket)->s = ft_strdup(s);
		(*rocket)->path = ft_strdup(s);
		(*rocket)->next = NULL;
	}
}

char	*create_path(char const *head, char const *path)
{
	char	*temp;
	char	*res;
	int		size;

	temp = NULL;
	size = strlen(head);
	if (head[size - 1] != '/')
		temp = ft_strjoin(head, "/");
	if (temp == NULL)
		res = ft_strjoin(head, path);
	else
		res = ft_strjoin(temp, path);
	if (temp != NULL)
		free(temp);
	return (res);
}

void	ft_make_chain(t_a **rocket, char *name, char *path, t_b *keys)
{
	t_a	*elem;

	if (*rocket != NULL)
	{
		elem = (t_a *)malloc(sizeof(t_a));
		elem->s = ft_strdup(name);
		elem->path = ft_strdup(path);
		elem->next = NULL;
		ft_sort_list(rocket, elem, keys);
	}
	if (*rocket == NULL)
	{
		*rocket = (t_a *)malloc(sizeof(t_a));
		(*rocket)->s = ft_strdup(name);
		(*rocket)->path = ft_strdup(path);
		(*rocket)->next = NULL;
	}
}
