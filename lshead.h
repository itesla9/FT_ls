#ifndef LSHEAD_H
# define LSHEAD_H

# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/dir.h>
# include <sys/stat.h>
# include <time.h>
# include <errno.h>
# include <grp.h>
# include <pwd.h>
# include <unistd.h>
# include <sys/xattr.h>
# include "libft/libft.h"
# include "ft_printf/header.h"

typedef struct  s_a
{
    char *s;
    char *path;
    struct s_a *next;

}               t_a;

typedef struct  s_b
{
    int l;
    int rr;
    int a;
    int r;
    int t;
    int error;
}               t_b;

typedef struct  s_c
{
    int sum;
    int one_arg;
}               t_c;

typedef struct s_d
{
    int total;
    int links;
    int who;
    int  group;
    int bytes;
}              t_d;

void    ft_parsing(int argc, char **argv, t_b *keys, t_a **start);
int     ft_hand_not_existing_files(t_a **start, t_b *keys, t_a **s_n_f);
void    ft_test(t_a *start);  // for testing
void    ft_stream(t_a **start, t_b *keys, t_c *flags);
void    ft_recurtion(char *path, t_b *keys, t_c *flags);
void    ft_make_chain(t_a **rocket, char *name, char *path, t_b *keys);
void    ft_final_print(t_a *rocket, t_b *keys, t_c *flags, char *path);
void    ft_general(char *path, t_b *keys, t_c *flags);
void    ft_print_l(t_a *rocket, t_b *keys);
char	**ft_strsplit(char const *s, char c);
void    ft_clean_list(t_a **rocket);
char    *ft_strjoin(const char *s1, const char *s2);
char    *create_path(char const *head, char const *path);
void    ft_find_bytes(t_a *rocket, t_b *keys, t_d *alignment);
void    ft_find_group(t_a *rocket, t_b *keys, t_d *alignment);
void    ft_find_who(t_a *rocket, t_b *keys, t_d *alignment);
void    ft_find_links(t_a *rocket, t_b *keys, t_d *alignment);
void    ft_find_total(t_a *rocket, t_b *keys, t_d *alignment);
void    ft_t(t_a **rocket);
long int     ft_compare_time_rt(char *head, char *new);
void    ft_print_error(char *s);
void    ft_sort_list(t_a **start, t_a *elem, t_b *keys);
void	ft_del_from_list_case_two(t_a **start, t_a **p);
void	ft_del_from_list_case_one(t_a **start, t_a **p);
int		ft_size_of_list(t_a *start);
int		ft_size_of_list2(t_a *start);
void	ft_clean_year(char *s);
void	ft_delete_sek(char *s);
void	ft_clean_list(t_a **start);
void	ft_make_alignment(t_a *rocket, t_b *keys, t_d *alignment);
void    ft_make_chain_not_files(t_a **rocket, char *s);
void    ft_add_to_sort(t_a *ptr, t_a **elem, t_a *crawler, t_a **start);
void    ft_print_not_files(t_a *start);
void	ft_not_files(t_a **start_not_files);
void	ft_permissions(struct stat buf, t_a *rocket, t_d *alignment);
void	ft_del_from_list(t_a **start, t_a **p);
void		ft_add_to_sort(t_a *ptr, t_a **elem, t_a *crawler, t_a **start);
void		ft_check_list_simple(t_a **start, t_a *elem);
#endif