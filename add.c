/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   case.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/22 14:37:14 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/24 19:37:23 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void	ft_del_from_list_case_two(t_a **start, t_a **p)
{
	t_a *ptr;

	ptr = *start;
	while (ptr->next != *p)
		ptr = ptr->next;
	ptr->next = (*p)->next;
	free((*p)->s);
	free((*p)->path);
	free(*p);
	*p = ptr->next;
}

void	ft_del_from_list_case_one(t_a **start, t_a **p)
{
	t_a *tmp;

	if ((*p)->next == NULL)
	{
		free((*start)->s);
		free((*start)->path);
		free(*start);
		*start = NULL;
		*p = NULL;
	}
	else
	{
		*start = (*p)->next;
		tmp = *p;
		*p = (*p)->next;
		free(tmp->s);
		free(tmp->path);
		free(tmp);
	}
}

void	ft_clean_list(t_a **start)
{
	t_a *rocket;
	t_a *ptr;

	rocket = *start;
	ptr = rocket;
	if (!rocket)
		return ;
	while (rocket)
	{
		rocket = rocket->next;
		free(ptr->s);
		free(ptr->path);
		free(ptr);
		ptr = rocket;
	}
}

void	ft_delete_sek(char *s)
{
	char	**arr;
	int		i;

	i = 0;
	arr = ft_strsplit(s, ':');
	ft_printf("%s:%s", arr[0], arr[1]);
	while (arr[i])
	{
		free(arr[i]);
		i++;
	}
	free(arr);
}

void	ft_clean_year(char *s)
{
	char			**mass;
	int				i;
	static	char	*year;
	time_t			t;

	s = ft_strdup(s);
	time(&t);
	year = &((ctime(&t))[20]);
	i = 0;
	mass = ft_strsplit(s, ' ');
	ft_printf("%s %2s ", mass[1], mass[2]);
	if (ft_strcmp(year, mass[4]) != 0)
		ft_printf(" %c%c%c%c", mass[4][0], mass[4][1], mass[4][2], mass[4][3]);
	else
		ft_delete_sek(mass[3]);
	while (mass[i])
	{
		free(mass[i]);
		i++;
	}
	free(s);
	free(mass);
}
