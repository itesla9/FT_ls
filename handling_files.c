/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handling_files.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/22 14:26:50 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/24 19:40:25 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void	ft_print_l_files(t_a *rocket, t_b *keys)
{
	struct stat buf;
	t_d			*align;

	align = (t_d*)malloc(sizeof(t_d));
	ft_make_alignment(rocket, keys, align);
	while (rocket)
	{
		lstat(rocket->path, &buf);
		if (keys->a == 1)
			ft_permissions(buf, rocket, align);
		else
		{
			if (rocket->s[0] != '.')
				ft_permissions(buf, rocket, align);
		}
		rocket = rocket->next;
	}
	free(align);
}

void	ft_print_files(t_a *files_start, t_b *keys)
{
	t_a *ptr;

	if (keys->l == 1)
		ft_print_l_files(files_start, keys);
	else
	{
		ptr = files_start;
		while (ptr)
		{
			ft_printf("%s\n", ptr->s);
			ptr = ptr->next;
		}
	}
}

t_a		*ft_make_list_of_files(t_a *files_start, t_a *p)
{
	t_a *elem;
	t_a *ptr;

	if (files_start != NULL)
	{
		ptr = files_start;
		elem = (t_a *)malloc(sizeof(t_a));
		elem->s = ft_strdup(p->s);
		elem->path = ft_strdup(p->s);
		elem->next = NULL;
		while (ptr->next)
			ptr = ptr->next;
		ptr->next = elem;
	}
	if (files_start == NULL)
	{
		files_start = (t_a *)malloc(sizeof(t_a));
		files_start->s = ft_strdup(p->s);
		files_start->path = ft_strdup(p->path);
		files_start->next = NULL;
	}
	return (files_start);
}

int		ft_handling_files(t_a **start, t_b *keys, int flag_for_print)
{
	t_a			*p;
	t_a			*files_start;
	struct stat buf;

	files_start = NULL;
	p = *start;
	while (p)
	{
		lstat(p->s, &buf);
		if (S_ISREG(buf.st_mode))
		{
			flag_for_print = 1;
			files_start = ft_make_list_of_files(files_start, p);
			ft_del_from_list(start, &p);
			continue ;
		}
		p = p->next;
	}
	if (files_start != NULL)
	{
		ft_print_files(files_start, keys);
		ft_clean_list(&files_start);
	}
	return (flag_for_print);
}

int		ft_hand_not_existing_files(t_a **start, t_b *keys, t_a **s_n_f)
{
	t_a			*p;
	int			status;
	struct stat	buf;
	int			flag_for_print;

	flag_for_print = 0;
	p = *start;
	while (p)
	{
		status = lstat(p->s, &buf);
		if (status == -1)
		{
			ft_make_chain_not_files(s_n_f, p->s);
			flag_for_print = 1;
			ft_del_from_list(start, &p);
			continue ;
		}
		p = p->next;
	}
	if (*s_n_f != NULL)
		ft_not_files(s_n_f);
	if (*start != NULL)
		ft_handling_files(start, keys, flag_for_print);
	return (flag_for_print);
}
