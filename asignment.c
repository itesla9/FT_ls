/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asignment.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/21 16:45:50 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/21 18:08:37 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void	ft_find_bytes(t_a *rocket, t_b *keys, t_d *align)
{
	struct stat	buf;
	int			size;

	size = 0;
	align->bytes = 0;
	while (rocket)
	{
		lstat(rocket->path, &buf);
		if (keys->a == 1)
			align->bytes = (align->bytes < buf.st_size) ?
				buf.st_size : align->bytes;
		if (keys->a != 1)
		{
			if (rocket->s[0] != '.')
				align->bytes = (align->bytes < buf.st_size) ?
					buf.st_size : align->bytes;
		}
		rocket = rocket->next;
	}
	while (align->bytes)
	{
		size++;
		align->bytes = align->bytes / 10;
	}
	align->bytes = size;
}

void	ft_find_group(t_a *rocket, t_b *keys, t_d *align)
{
	struct stat		buf;
	struct group	*r;

	align->group = 0;
	while (rocket)
	{
		lstat(rocket->path, &buf);
		r = getgrgid(buf.st_gid);
		if (keys->a == 1)
			align->group = (align->group < (int)ft_strlen(r->gr_name)) ?
				(int)strlen(r->gr_name) : align->group;
		if (keys->a != 1)
		{
			if (rocket->s[0] != '.')
				align->group = (align->group < (int)ft_strlen(r->gr_name)) ?
					(int)strlen(r->gr_name) : align->group;
		}
		rocket = rocket->next;
	}
}

void	ft_find_who(t_a *rocket, t_b *keys, t_d *align)
{
	struct stat		buf;
	struct passwd	*owner;

	align->who = 0;
	while (rocket)
	{
		lstat(rocket->path, &buf);
		owner = getpwuid(buf.st_uid);
		if (keys->a == 1)
			align->who = (align->who < (int)ft_strlen(owner->pw_name)) ?
				(int)strlen(owner->pw_name) : align->who;
		if (keys->a != 1)
		{
			if (rocket->s[0] != '.')
				align->who = (align->who < (int)ft_strlen(owner->pw_name)) ?
					(int)strlen(owner->pw_name) : align->who;
		}
		rocket = rocket->next;
	}
}

void	ft_find_links(t_a *rocket, t_b *keys, t_d *align)
{
	struct stat	buf;
	int			size;

	size = 0;
	align->links = 0;
	while (rocket)
	{
		lstat(rocket->path, &buf);
		if (keys->a == 1)
			align->links = (align->links < buf.st_nlink) ?
				(int)buf.st_nlink : align->links;
		if (keys->a != 1)
		{
			if (rocket->s[0] != '.')
				align->links = (align->links < buf.st_nlink) ?
					(int)buf.st_nlink : align->links;
		}
		rocket = rocket->next;
	}
	while (align->links != 0)
	{
		size++;
		align->links = align->links / 10;
	}
	align->links = size;
}

void	ft_find_total(t_a *rocket, t_b *keys, t_d *align)
{
	struct stat buf;

	align->total = 0;
	while (rocket)
	{
		lstat(rocket->path, &buf);
		if (keys->a == 1)
			align->total = align->total + (int)buf.st_blocks;
		if (keys->a != 1)
		{
			if (rocket->s[0] != '.')
				align->total = align->total + (int)buf.st_blocks;
		}
		rocket = rocket->next;
	}
}
