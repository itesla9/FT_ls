/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 15:53:52 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/05 13:41:22 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (s1 && s2)
	{
		if (n == 0)
			return (1);
		while (s1[i] || i < n)
		{
			if (*((unsigned char *)s1 + i) != *((unsigned char *)s2 + i))
				return (0);
			i++;
			if (i == n)
				break ;
		}
	}
	return (1);
}
