/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strnstr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 12:56:37 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/02 16:51:35 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_strnstr(const char *s1, const char *s2, size_t len)
{
	size_t		i;
	size_t		j;
	const char	*fin;

	i = 0;
	j = 0;
	if (s2[j] == '\0')
		return ((char *)s1);
	while (s1[i])
	{
		if (s1[i] == s2[j])
			fin = &s1[i];
		j = 0;
		while (s1[i + j] == s2[j] && (i + j) < len)
		{
			j++;
			if (j == ft_strlen(s2))
				return ((char*)fin);
		}
		i++;
	}
	return (NULL);
}
