/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 17:46:37 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/02 17:20:35 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_numsize(int n)
{
	int	j;
	int	i;

	j = 0;
	i = 0;
	if (n <= 0)
		j++;
	while (n != 0)
	{
		n = n / 10;
		i++;
	}
	return (i + j);
}

static	int		ft_additional(int n, char *fresh)
{
	if (n <= 0)
	{
		if (n == 0)
			fresh[0] = '0';
		else if (n == -2147483648)
		{
			fresh[0] = '-';
			fresh[1] = '2';
			return (147483648);
		}
		else
			fresh[0] = '-';
	}
	return (-n);
}

char			*ft_itoa(int n)
{
	char			*fresh;
	int				len;

	len = ft_numsize(n);
	fresh = (char *)malloc(len + 1);
	if (!fresh)
		return (NULL);
	fresh[len] = '\0';
	if (n <= 0)
		n = ft_additional(n, fresh);
	while (--len > -1 && n)
	{
		fresh[len] = n % 10 + '0';
		n = n / 10;
	}
	return (fresh);
}
