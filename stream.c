/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/22 16:38:21 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/22 16:45:00 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void		ft_recurtion_continue(t_a **rocket, t_b *keys, t_c *flags)
{
	struct stat buf;
	t_a			*ptr;

	ptr = *rocket;
	while (ptr)
	{
		lstat(ptr->path, &buf);
		if (S_ISDIR(buf.st_mode) && (ft_strcmp(ptr->s, ".") != 0
									&& ft_strcmp(ptr->s, "..") != 0))
			ft_recurtion(ptr->path, keys, flags);
		ptr = ptr->next;
	}
	if (*rocket != NULL)
		ft_clean_list(rocket);
}

void		ft_recurtion(char *path, t_b *keys, t_c *flags)
{
	DIR				*directory_stream;
	t_a				*rocket;
	struct dirent	*x;
	struct stat		buf;
	char			*s;

	rocket = NULL;
	directory_stream = opendir(path);
	while (directory_stream && (x = readdir(directory_stream)) != NULL)
	{
		s = create_path(path, x->d_name);
		lstat(s, &buf);
		if (S_ISREG(buf.st_mode) || S_ISDIR(buf.st_mode) ||
			S_ISLNK(buf.st_mode))
		{
			ft_make_chain(&rocket, x->d_name, s, keys);
			free(s);
		}
	}
	ft_final_print(rocket, keys, flags, path);
	ft_recurtion_continue(&rocket, keys, flags);
	if (directory_stream)
		closedir(directory_stream);
}

void		ft_general(char *path, t_b *keys, t_c *flags)
{
	DIR				*directory_stream;
	t_a				*rocket;
	struct dirent	*x;
	struct stat		buf;
	char			*s;

	rocket = NULL;
	directory_stream = opendir(path);
	while (directory_stream && (x = readdir(directory_stream)) != NULL)
	{
		s = create_path(path, x->d_name);
		lstat(s, &buf);
		if (S_ISREG(buf.st_mode) || S_ISDIR(buf.st_mode) ||
			S_ISLNK(buf.st_mode))
			ft_make_chain(&rocket, x->d_name, s, keys);
		free(s);
	}
	ft_final_print(rocket, keys, flags, path);
	ft_clean_list(&rocket);
	closedir(directory_stream);
}

void		ft_stream(t_a **start, t_b *keys, t_c *flags)
{
	t_a	*p;

	p = *start;
	while (p)
	{
		if (keys->rr == 1)
			ft_recurtion(p->path, keys, flags);
		else
			ft_general(p->path, keys, flags);
		p = p->next;
	}
}
