/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlen.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 12:51:15 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/01 19:43:59 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_strlen2(const char *s)
{
	int	amount;

	amount = 0;
	if (!s)
		return (0);
	while (s[amount])
		amount++;
	return (amount);
}
