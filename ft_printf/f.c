/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/20 16:18:59 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/04 14:53:15 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		ft_put_modifier(t_struct *ps)
{
	if (ps->a == 0)
		ps->a = ggg(ps);
	if (ps->a != 0)
	{
		if (ps->a < (ggg(ps)))
			ps->a = ggg(ps);
	}
}

void		ft_flags(t_struct *ps)
{
	if (*(ps->ptr) == 32)
		ps->space = 1;
	if (*(ps->ptr) == 43)
		ps->plus = 1;
	if (*(ps->ptr) == 45)
		ps->hyphen = 1;
	if (*(ps->ptr) == 35)
		ps->shark = 1;
	if (*(ps->ptr) == 48)
		ps->zero = 1;
}

void		ft_fill_struct3(t_struct *ps)
{
	if (*(ps->ptr) >= 48 && *(ps->ptr) <= 57 && ps->flag > 0)
	{
		ps->precision = ft_atoi(ps->ptr);
		while (*(ps->ptr + 1) >= 48 && *(ps->ptr + 1) <= 57)
			(ps->ptr)++;
	}
}

void		ft_fill_struct2(t_struct *ps, va_list ap)
{
	if (*(ps->ptr) >= 48 && *(ps->ptr) <= 57 && ps->flag == 0)
	{
		ps->width = ft_atoi(ps->ptr);
		while (*(ps->ptr + 1) >= 48 && *(ps->ptr + 1) <= 57)
			(ps->ptr)++;
		return ;
	}
	if (*(ps->ptr) == '.')
		ps->precision = 0;
	if (*(ps->ptr) == '.' && ((*(ps->ptr + 1) >= 48 && *(ps->ptr + 1) <= 57) ||
		*(ps->ptr + 1) == 42))
	{
		(ps->flag)++;
		(ps->ptr)++;
	}
	if (*(ps->ptr) == 42 && ps->flag > 0)
	{
		ps->precision = va_arg(ap, int);
		if (ps->precision < 0)
			ps->precision = -1;
	}
	ft_fill_struct3(ps);
}

void		ft_fill_struct(t_struct *ps, va_list ap)
{
	if (*(ps->ptr) == 'h' || *(ps->ptr) == 'l' || *(ps->ptr) == 'j' ||
		*(ps->ptr) == 'z')
		ft_put_modifier(ps);
	if (*(ps->ptr) == 32 || *(ps->ptr) == 43 || *(ps->ptr) == 45 ||
		*(ps->ptr) == 35 || *(ps->ptr) == 48)
	{
		ft_flags(ps);
		return ;
	}
	if (*(ps->ptr) == 42 && ps->flag == 0)
	{
		ps->width = va_arg(ap, int);
		if (ps->width < 0)
		{
			ps->width = -ps->width;
			ps->hyphen = 1;
		}
		if (*(ps->ptr + 1) >= 48 && *(ps->ptr + 1) <= 57)
			(ps->ptr)++;
	}
	ft_fill_struct2(ps, ap);
}
