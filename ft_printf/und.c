/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   und.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 12:46:47 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/01 17:44:31 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_hyphen1_0_unsigned(t_struct *ps, unsigned long long xxx,
							int reserve, int reserve_int)
{
	if (ps->hyphen == 1)
	{
		while (reserve_int > 0)
		{
			ft_putchar('0');
			reserve_int--;
		}
		ft_putnbr2(xxx, ps);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		while (reserve_int > 0)
		{
			ft_putchar('0');
			reserve_int--;
		}
		ft_putnbr2(xxx, ps);
	}
}

void	ft_hyphen2_0_unsigned(t_struct *ps, unsigned long long xxx, int reserve)
{
	if (ps->hyphen == 1)
	{
		ft_putnbr2(xxx, ps);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		ft_putnbr2(xxx, ps);
	}
}

void	ft_hyphen3_0_unsigned(t_struct *ps, unsigned long long xxx, int reserve)
{
	if (ps->zero == 0)
	{
		if (ps->hyphen == 0)
		{
			ft_putspace(reserve, ps);
			ft_putnbr2(xxx, ps);
		}
		if (ps->hyphen != 0)
		{
			ft_putnbr2(xxx, ps);
			ft_putspace(reserve, ps);
		}
	}
	if (ps->zero == 1)
	{
		if (ps->hyphen == 0)
		{
			ft_putspace(reserve, ps);
			ft_putnbr2(xxx, ps);
		}
	}
}

void	ft_widthbig_und(t_struct *ps, unsigned long long xxx, int len)
{
	int	reserve_int;
	int	reserve;

	if (ps->precision > len)
		reserve_int = ps->precision - len;
	else
		reserve_int = 0;
	reserve = ft_reserve_full(ps, len);
	if (ps->precision != -1)
	{
		if (ps->precision > len)
			ft_hyphen1_0_unsigned(ps, xxx, reserve, reserve_int);
		if (ps->precision <= len && ps->precision >= 0)
			ft_hyphen2_0_unsigned(ps, xxx, reserve);
	}
	if (ps->precision == -1)
		ft_hyphen3_0_unsigned(ps, xxx, reserve);
}

void	ft_widthles_und(t_struct *ps, unsigned long long xxx, int len)
{
	int	repeat;

	if (ps->precision > len)
	{
		repeat = ps->precision - len;
		while (repeat > 0)
		{
			ft_putchar('0');
			repeat--;
		}
		ft_putnbr2(xxx, ps);
	}
	else
		ft_putnbr2(xxx, ps);
}
