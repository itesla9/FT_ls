/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   x2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 18:39:56 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/03 20:29:27 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_res_int(int reserve_int)
{
	while (reserve_int > 0)
	{
		ft_putchar('0');
		reserve_int--;
	}
}

void	ft_hyphen1_0_x(t_struct *ps, char *fresh, int reserve, int reserve_int)
{
	if (ps->hyphen == 1)
	{
		if (ps->shark == 1 && *(ps->ptr) == 'x')
			ft_putstr("0x");
		if (ps->shark == 1 && *(ps->ptr) == 'X')
			ft_putstr("0X");
		ft_res_int(reserve_int);
		ft_putstr(fresh);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		if (ps->shark == 1 && *(ps->ptr) == 'x')
			ft_putstr("0x");
		if (ps->shark == 1 && *(ps->ptr) == 'X')
			ft_putstr("0X");
		ft_res_int(reserve_int);
		ft_putstr(fresh);
	}
}

void	ft_hyphen2_0_x(t_struct *ps, char *fresh, int reserve)
{
	if (ps->hyphen == 1)
	{
		if (ps->shark == 1)
			ft_putstr("0x");
		ft_putstr(fresh);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		if (ps->shark == 1)
			ft_putstr("0x");
		if (fresh[0] != '0')
			ft_putstr(fresh);
	}
}

void	ft_hyphen_3_02_x(t_struct *ps, char *fresh, int reserve)
{
	if (ps->hyphen == 0)
	{
		if (ps->shark == 1 && *(ps->ptr) == 'x' && fresh[0] != '0')
			ft_putstr("0x");
		if (ps->shark == 1 && *(ps->ptr) == 'X' && fresh[0] != '0')
			ft_putstr("0X");
		ft_putspace(reserve, ps);
		ft_putstr(fresh);
	}
}

void	ft_hyphen3_0_x(t_struct *ps, char *fresh, int reserve)
{
	if (ps->precision == -1 && fresh[0] == '0')
		reserve = reserve - 1;
	if (ps->zero == 0)
	{
		if (ps->hyphen == 0)
		{
			ft_putspace(reserve, ps);
			if (ps->shark == 1 && *(ps->ptr) == 'x' && fresh[0] != '0')
				ft_putstr("0x");
			if (ps->shark == 1 && *(ps->ptr) == 'X' && fresh[0] != '0')
				ft_putstr("0X");
			ft_putstr(fresh);
		}
		if (ps->hyphen == 1)
		{
			if (ps->shark == 1 && *(ps->ptr) == 'x' && fresh[0] != '0')
				ft_putstr("0x");
			if (ps->shark == 1 && *(ps->ptr) == 'X' && fresh[0] != '0')
				ft_putstr("0X");
			ft_putstr(fresh);
			ft_putspace(reserve, ps);
		}
	}
	if (ps->zero == 1)
		ft_hyphen_3_02_x(ps, fresh, reserve);
}
