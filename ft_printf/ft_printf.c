/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 14:15:55 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/04 12:35:52 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		ft_extra(t_struct *ps)
{
	int	reserve;

	reserve = ps->width - 1;
	if (ps->width > 1)
	{
		if (ps->hyphen == 1)
		{
			ft_putchar(*(ps->ptr));
			ft_extra_add(ps, reserve);
		}
		if (ps->hyphen == 0)
		{
			ft_extra_add(ps, reserve);
			ft_putchar(*(ps->ptr));
		}
	}
	else
		ft_putchar(*(ps->ptr));
	(ps->ptr)++;
}

int			ft_findout(t_struct *ps)
{
	if (*(ps->ptr) != 's' && *(ps->ptr) != 'S' && *(ps->ptr) != 'p' &&
		*(ps->ptr) != 'd' && *(ps->ptr) != 'D' && *(ps->ptr) != 'i' &&
		*(ps->ptr) != 'o' && *(ps->ptr) != 'O' && *(ps->ptr) != 'u' &&
		*(ps->ptr) != 'U' && *(ps->ptr) != 'x' && *(ps->ptr) != 'X' &&
		*(ps->ptr) != 'c' && *(ps->ptr) != 'C' && *(ps->ptr) != 'l' &&
		*(ps->ptr) != 'h' && *(ps->ptr) != 'j' && *(ps->ptr) != 'z' &&
		*(ps->ptr) != '\0' && *(ps->ptr) != 'n')
		return (1);
	return (0);
}

t_struct	*first_step(t_struct *ps, va_list ap)
{
	ps->plus = 0;
	ps->hyphen = 0;
	ps->zero = 0;
	ps->shark = 0;
	ps->space = 0;
	ps->width = 0;
	ps->a = 0;
	ps->flag = 0;
	ps->precision = -1;
	while (isflag(ps) || ismodifier(ps) || isnum(ps))
	{
		ft_fill_struct(ps, ap);
		(ps->ptr)++;
	}
	ft_cheak_struct(ps);
	return (ps);
}

void		ft_printf2(t_struct *ps, va_list ap)
{
	while (*(ps->ptr))
	{
		if (*(ps->ptr) != '%')
			ft_putchar(*(ps->ptr));
		if (*(ps->ptr) == '%')
		{
			(ps->ptr)++;
			if (*(ps->ptr) != '%' && *(ps->ptr) != '\0')
			{
				ps = first_step(ps, ap);
				if (ft_findout(ps) == 1)
				{
					ft_extra(ps);
					continue ;
				}
				ft_adjust(ps, ap);
			}
			if (*(ps->ptr) == '%')
				ft_putchar('%');
		}
		if (*(ps->ptr) == '\0')
			break ;
		(ps->ptr)++;
	}
}

int			ft_printf(const char *restrict format, ...)
{
	va_list		ap;
	t_struct	*ps;

	ps = (t_struct *)malloc(sizeof(t_struct));
	ps->ptr = (char*)format;
	g_stars = 0;
	va_start(ap, format);
	ft_printf2(ps, ap);
	free(ps);
	return (g_stars);
}
