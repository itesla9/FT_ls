/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 18:09:32 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/02 20:42:06 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_len_base(long long xxx, int base)
{
	int i;
	int j;

	i = 0;
	j = 0;
	if (xxx <= 0)
		i++;
	while (xxx != 0)
	{
		j++;
		xxx /= base;
	}
	return (i + j);
}

int		ft_len_base2(unsigned long long xxx, int base)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (xxx == 0)
		i++;
	while (xxx != 0)
	{
		j++;
		xxx /= base;
	}
	return (i + j);
}

char	*ft_magic(char *fresh)
{
	int	i;

	i = 0;
	while (fresh[i])
	{
		fresh[i] = ft_tolower(fresh[i]);
		i++;
	}
	return (fresh);
}

char	*ft_itoa_base(long long xxx, int base, int len)
{
	char	*fresh;
	char	*itoa;

	itoa = "0123456789ABCDEF";
	len = ft_len_base(xxx, base);
	fresh = (char *)malloc(sizeof(char) * (len + 1));
	if (!fresh)
		return (NULL);
	fresh[len] = 0;
	if (xxx == 0)
		fresh[0] = '0';
	if (xxx < 0 && base == 10 && xxx != -2147483648)
	{
		fresh[0] = '-';
		xxx = -xxx;
	}
	if (xxx > 0)
	{
		while (--len > -1 && xxx)
		{
			fresh[len] = itoa[xxx % base];
			xxx /= base;
		}
	}
	return (fresh);
}

char	*ft_itoa_base2(unsigned long long xxx, int base)
{
	int		len;
	char	*fresh;
	char	*itoa;

	itoa = "0123456789ABCDEF";
	len = ft_len_base2(xxx, base);
	fresh = (char *)malloc(sizeof(char) * (len + 1));
	if (!fresh)
		return (NULL);
	fresh[len] = 0;
	if (xxx == 0)
		fresh[0] = '0';
	if (xxx > 0)
	{
		while (--len > -1 && xxx)
		{
			fresh[len] = itoa[xxx % base];
			xxx /= base;
		}
	}
	return (fresh);
}
