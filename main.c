/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/22 14:34:28 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/24 19:41:54 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void	ft_make_alignment(t_a *rocket, t_b *keys, t_d *alignment)
{
	ft_find_total(rocket, keys, alignment);
	ft_find_links(rocket, keys, alignment);
	ft_find_who(rocket, keys, alignment);
	ft_find_group(rocket, keys, alignment);
	ft_find_bytes(rocket, keys, alignment);
}

int		ft_size_of_list2(t_a *start)
{
	t_a *ptr;
	int size;

	size = 0;
	ptr = start;
	while (ptr)
	{
		if (ptr->s[0] != '.')
			size++;
		ptr = ptr->next;
	}
	return (size);
}

int		ft_size_of_list(t_a *start)
{
	return (start ? 1 + ft_size_of_list(start->next) : 0);
}

t_b		*ft_define_keys(t_b *keys)
{
	keys = (t_b *)malloc(sizeof(t_b));
	keys->l = 0;
	keys->rr = 0;
	keys->a = 0;
	keys->r = 0;
	keys->t = 0;
	keys->error = 0;
	return (keys);
}

int		main(int argc, char **argv)
{
	t_a *start;
	t_b *keys;
	t_c *flags;
	t_a *s_n_f;

	keys = NULL;
	s_n_f = NULL;
	flags = (t_c *)malloc(sizeof(t_c));
	keys = ft_define_keys(keys);
	ft_parsing(argc, argv, keys, &start);
	if (start == NULL)
	{
		start = (t_a *)malloc(sizeof(t_a));
		start->s = ".";
		start->path = ".";
		start->next = NULL;
	}
	if (keys->error != 1)
	{
		if (start != NULL)
			flags->one_arg = ft_hand_not_existing_files(&start, keys, &s_n_f);
		flags->sum = ft_size_of_list(start);
		ft_stream(&start, keys, flags);
	}
	//while(1);
	return (0);
}
