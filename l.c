/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   l.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/22 15:49:50 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/24 19:41:24 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void	ft_user(struct stat buf)
{
	if (S_ISDIR(buf.st_mode))
		ft_printf((S_ISDIR(buf.st_mode)) ? "d" : "-");
	if (S_ISLNK(buf.st_mode))
		ft_printf((S_ISLNK(buf.st_mode)) ? "l" : "-");
	if (S_ISREG(buf.st_mode))
		ft_printf("-");
	ft_printf((buf.st_mode & S_IRUSR) ? "r" : "-");
	ft_printf((buf.st_mode & S_IWUSR) ? "w" : "-");
	if (buf.st_mode & S_IXUSR)
	{
		if (buf.st_mode & S_ISUID)
			ft_printf("s");
		else
			ft_printf("x");
	}
	else
	{
		if (buf.st_mode & S_ISUID)
			ft_printf("S");
		else
			ft_printf("-");
	}
}

void	ft_group(struct stat buf)
{
	ft_printf((buf.st_mode & S_IRGRP) ? "r" : "-");
	ft_printf((buf.st_mode & S_IWGRP) ? "w" : "-");
	if (buf.st_mode & S_IXGRP)
	{
		if (buf.st_mode & S_ISGID)
			ft_printf("s");
		else
			ft_printf("x");
	}
	else
	{
		if (buf.st_mode & S_ISGID)
			ft_printf("S");
		else
			ft_printf("-");
	}
}

int		ft_others(struct stat buf, t_a *rocket, int extra_space)
{
	ft_printf((buf.st_mode & S_IROTH) ? "r" : "-");
	ft_printf((buf.st_mode & S_IWOTH) ? "w" : "-");
	if (buf.st_mode & S_IXOTH)
	{
		if (buf.st_mode & S_ISVTX)
			ft_printf("t");
		else
			ft_printf("x");
	}
	else
	{
		if (buf.st_mode & S_ISVTX)
			ft_printf("T");
		else
			ft_printf("-");
	}
	if (listxattr(rocket->path, 0, 0, 0) > 0)
	{
		ft_printf("@");
		extra_space++;
	}
	return (extra_space);
}

void	ft_permissions(struct stat buf, t_a *rocket, t_d *alignment)
{
	struct group	*result;
	struct passwd	*owner;
	char			link[1024];
	int				extra_space;

	extra_space = 0;
	result = getgrgid(buf.st_gid);
	owner = getpwuid(buf.st_uid);
	ft_user(buf);
	ft_group(buf);
	extra_space = ft_others(buf, rocket, extra_space);
	ft_printf("%*d ", alignment->links + 2 - extra_space, buf.st_nlink);
	ft_printf("%*s  ", alignment->who, owner->pw_name);
	ft_printf("%*s  ", alignment->group, result->gr_name);
	ft_printf("%*lld ", alignment->bytes, buf.st_size);
	ft_clean_year(ctime(&buf.st_mtimespec.tv_sec));
	if (S_ISLNK(buf.st_mode))
	{
		readlink(rocket->path, link, 1024);
		ft_printf(" %s -> %s", rocket->s, link);
	}
	else
		ft_printf(" %s", rocket->s);
	ft_printf("\n");
}

void	ft_print_l(t_a *rocket, t_b *keys)
{
	struct stat buf;
	t_d			*align;

	align = (t_d*)malloc(sizeof(t_d));
	ft_make_alignment(rocket, keys, align);
	if (rocket)
	{
		if (ft_size_of_list2(rocket) != 0)
			ft_printf("total %d\n", align->total);
	}
	while (rocket)
	{
		lstat(rocket->path, &buf);
		if (keys->a == 1)
			ft_permissions(buf, rocket, align);
		else
		{
			if (rocket->s[0] != '.')
				ft_permissions(buf, rocket, align);
		}
		rocket = rocket->next;
	}
	free(align);
}
