/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/23 20:03:45 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/23 20:06:22 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

long int	ft_comparison_time(char *head, char *new)
{
	struct stat start;
	struct stat elem;

	lstat(head, &start);
	lstat(new, &elem);
	if (start.st_mtimespec.tv_sec == elem.st_mtimespec.tv_sec)
	{
		if (start.st_mtimespec.tv_nsec != elem.st_mtimespec.tv_nsec)
			return (start.st_mtimespec.tv_nsec - elem.st_mtimespec.tv_nsec);
		if (ft_strcmp(head, new) > 0)
			return (-1);
		else
			return (1);
	}
	else if (start.st_mtimespec.tv_sec != elem.st_mtimespec.tv_sec)
		return (start.st_mtimespec.tv_sec - elem.st_mtimespec.tv_sec);
	return (0);
}

void		ft_check_list_rt(t_a **start, t_a *elem)
{
	t_a *ptr;
	t_a *crawler;

	ptr = *start;
	crawler = *start;
	while (ptr)
	{
		if (ft_comparison_time(ptr->path, elem->path) > 0)
		{
			ft_add_to_sort(ptr, &elem, crawler, start);
			return ;
		}
		ptr = ptr->next;
	}
	ptr = *start;
	while (ptr->next)
		ptr = ptr->next;
	ptr->next = elem;
}

void		ft_check_list_r(t_a **start, t_a *elem)
{
	t_a *ptr;
	t_a *crawler;

	ptr = *start;
	crawler = *start;
	while (ptr)
	{
		if (ft_strcmp(ptr->s, elem->s) < 0)
		{
			ft_add_to_sort(ptr, &elem, crawler, start);
			return ;
		}
		ptr = ptr->next;
	}
	ptr = *start;
	while (ptr->next)
		ptr = ptr->next;
	ptr->next = elem;
}

void		ft_check_list_t(t_a **start, t_a *elem)
{
	t_a *ptr;
	t_a *crawler;

	ptr = *start;
	crawler = *start;
	while (ptr)
	{
		if (ft_comparison_time(ptr->path, elem->path) < 0)
		{
			ft_add_to_sort(ptr, &elem, crawler, start);
			return ;
		}
		ptr = ptr->next;
	}
	ptr = *start;
	while (ptr->next)
		ptr = ptr->next;
	ptr->next = elem;
}

void		ft_sort_list(t_a **start, t_a *elem, t_b *keys)
{
	if (keys->r == 1 && keys->t == 1)
		ft_check_list_rt(start, elem);
	else if (keys->t == 1)
		ft_check_list_t(start, elem);
	else if (keys->r == 1)
		ft_check_list_r(start, elem);
	else
		ft_check_list_simple(start, elem);
}
