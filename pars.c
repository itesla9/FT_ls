/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/21 16:43:38 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/24 19:45:16 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void		ft_add_to_sort(t_a *ptr, t_a **elem, t_a *crawler, t_a **start)
{
	if (crawler == ptr)
	{
		(*elem)->next = ptr;
		*start = *elem;
	}
	else
	{
		while (crawler->next != ptr)
			crawler = crawler->next;
		crawler->next = *elem;
		(*elem)->next = ptr;
	}
}

void		ft_check_list_simple(t_a **start, t_a *elem)
{
	t_a *ptr;
	t_a *crawler;

	ptr = *start;
	crawler = *start;
	while (ptr)
	{
		if (ft_strcmp(ptr->s, elem->s) > 0)
		{
			ft_add_to_sort(ptr, &elem, crawler, start);
			return ;
		}
		ptr = ptr->next;
	}
	ptr = *start;
	while (ptr->next)
		ptr = ptr->next;
	ptr->next = elem;
}

void		ft_del_from_list(t_a **start, t_a **p)
{
	t_a *tmp;

	tmp = *start;
	if (*p == *start)
		ft_del_from_list_case_one(start, p);
	else if ((*p)->next == NULL)
	{
		while (tmp->next != *p)
			tmp = tmp->next;
		free((*p)->s);
		free((*p)->path);
		free(*p);
		tmp->next = NULL;
		*p = NULL;
	}
	else
		ft_del_from_list_case_two(start, p);
}

void		ft_fill_keys(t_b *keys, char *s)
{
	int i;

	i = 1;
	while (s[i])
	{
		if (s[i] == 'l')
			keys->l = 1;
		else if (s[i] == 'R')
			keys->rr = 1;
		else if (s[i] == 'a')
			keys->a = 1;
		else if (s[i] == 'r')
			keys->r = 1;
		else if (s[i] == 't')
			keys->t = 1;
		else
			keys->error = 1;
		i++;
	}
}

void		ft_parsing(int argc, char **argv, t_b *keys, t_a **start)
{
	int	i;
	int	flag;

	i = 1;
	flag = 0;
	*start = NULL;
	while (i < argc)
	{
		if (argv[i][0] == '-' && strcmp(argv[i], "-") != 0 && flag != 1)
			ft_fill_keys(keys, argv[i]);
		if (keys->error != 1)
		{
			if (argv[i][0] != '-' || strcmp(argv[i], "-") == 0)
			{
				flag = 1;
				ft_make_chain(start, argv[i], argv[i], keys);
			}
		}
		if (keys->error == 1)
		{
			ft_print_error(argv[i]);
			break ;
		}
		i++;
	}
}
