/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/23 20:09:58 by yteslenk          #+#    #+#             */
/*   Updated: 2017/06/24 13:12:30 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lshead.h"

void	ft_print_not_files(t_a *start)
{
	t_a *ptr;

	ptr = start;
	while (ptr)
	{
		ft_printf("ft_ls: %s: %s\n", ptr->s, strerror(errno));
		ptr = ptr->next;
	}
}

void	ft_print_error(char *s)
{
	int i;

	i = 1;
	while (s[i])
	{
		if (s[i] != 'l' && s[i] != 'R' && s[i] != 'a' &&
				s[i] != 'r' && s[i] != 't')
		{
			ft_printf("ft_ls: illegal option -- %c\n", s[i]);
			ft_printf("usage: ft_ls [-lRart] [file ...]\n");
			break ;
		}
		i++;
	}
}

int		ft_is_hidden(t_a *rocket)
{
	if (rocket->s[0] == '.')
		return (1);
	return (0);
}

void	ft_print_spaces(t_c *flags, char *path, int i)
{
	if (i == 0)
	{
		if (flags->one_arg == 1)
			ft_printf("\n");
		if (flags->sum >= 2 || flags->one_arg == 1)
			ft_printf("%s:\n", path);
	}
	else
		ft_printf("\n%s:\n", path);
}

void	ft_final_print(t_a *rocket, t_b *keys, t_c *flags, char *path)
{
	static int i;

	ft_print_spaces(flags, path, i);
	if (keys->l == 1)
		ft_print_l(rocket, keys);
	else if (keys->a == 1)
	{
		while (rocket)
		{
			ft_printf("%s\n", rocket->s);
			rocket = rocket->next;
		}
	}
	else
	{
		while (rocket)
		{
			if (!ft_is_hidden(rocket))
				ft_printf("%s\n", rocket->s);
			rocket = rocket->next;
		}
	}
	i++;
}
